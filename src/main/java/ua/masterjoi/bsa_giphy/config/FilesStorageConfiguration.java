package ua.masterjoi.bsa_giphy.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;


@Configuration
public class FilesStorageConfiguration {
    @Bean
    @Scope("singleton")
    public FilesStorage filesStorage(FilesStorageConfigurationProperties configurationProperties) {
        return new FilesStorage(configurationProperties.getCacheDirectory()
                , configurationProperties.getUsersDirectory());
    }
}
