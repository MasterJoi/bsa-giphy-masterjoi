package ua.masterjoi.bsa_giphy.config;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

@AllArgsConstructor
@Getter
public class FilesStorage {
        private final String cacheDirectory;
        private final String usersDirectory;

    public void createEmptyDirectories() {
        try {
            Files.createDirectories(Paths.get(cacheDirectory));
            Files.createDirectories(Paths.get(usersDirectory));
        } catch (IOException e) {
            throw new RuntimeException("Directories aren`t created!");
        }
    }
}
