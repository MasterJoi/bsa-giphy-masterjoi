package ua.masterjoi.bsa_giphy.config;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class GiphyApp {
    private final String url;
    private final String apiKey;
}

