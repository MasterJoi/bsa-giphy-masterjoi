package ua.masterjoi.bsa_giphy.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "data")
@Setter
@Getter
public class FilesStorageConfigurationProperties {
    private String cacheDirectory;
    private String usersDirectory;
}
