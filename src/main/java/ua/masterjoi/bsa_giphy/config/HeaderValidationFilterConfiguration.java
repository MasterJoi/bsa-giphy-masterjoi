package ua.masterjoi.bsa_giphy.config;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ua.masterjoi.bsa_giphy.filter.HeaderValidationFilter;

@Configuration
public class HeaderValidationFilterConfiguration {
    @Bean
    public FilterRegistrationBean<HeaderValidationFilter> headerValidatorFilter() {
        FilterRegistrationBean<HeaderValidationFilter> registrationBean = new FilterRegistrationBean<>();
        registrationBean.setFilter(new HeaderValidationFilter());
        registrationBean.addUrlPatterns("*");
        return registrationBean;
    }
}
