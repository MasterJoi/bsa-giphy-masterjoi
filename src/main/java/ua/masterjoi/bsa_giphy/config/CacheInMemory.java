package ua.masterjoi.bsa_giphy.config;

import lombok.Getter;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

@Getter
public class CacheInMemory {
    private final Map<String, Map<String, ArrayList<String>>> cacheDictionary;

    CacheInMemory() {
        cacheDictionary = new HashMap<>();
    }

    public Optional<String> getGifPath(String user_id, String query) {
        List<String> userGifsList;
        try {
            userGifsList = cacheDictionary.get(user_id).get(query);
        } catch (NullPointerException e) {
            return Optional.empty();
        }

        if (!userGifsList.isEmpty()) {
            int randomIndex = ThreadLocalRandom.current().nextInt(0, userGifsList.size());
            return Optional.of(userGifsList.get(randomIndex));
        } else {
            return Optional.empty();
        }
    }

    public void addGifToCache(String user_id, String query, String gifPath) {
        if (cacheDictionary.get(user_id) == null) {
            cacheDictionary.put(user_id, new HashMap<>() {
                {
                    put(query, new ArrayList<>() {{ add(gifPath); }});
                }
            });
        } else {
            if (cacheDictionary.get(user_id).get(query) == null) {
                cacheDictionary.get(user_id).put(query, new ArrayList<>() {{add(gifPath);}});
            } else {
                cacheDictionary.get(user_id).get(query).add(gifPath);
            }
        }
    }

    public void clearAllUserCache(String user_id) {
        if(!cacheDictionary.isEmpty())
        cacheDictionary.remove(user_id);
    }

    public void clearUserCacheByQuery(String user_id, String query) {
        if(!cacheDictionary.isEmpty())
        cacheDictionary.get(user_id).remove(query);
    }
}
