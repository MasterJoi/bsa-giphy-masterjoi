package ua.masterjoi.bsa_giphy.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class CacheInMemoryConfiguration {
    @Bean
    @Scope("singleton")
    public CacheInMemory cacheInMemory() {
        return new CacheInMemory();
    }
}
