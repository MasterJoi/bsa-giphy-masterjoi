package ua.masterjoi.bsa_giphy.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "giphy")
@Setter
@Getter
public class GiphyAppConfigurationProperties {
    private String url;
    private String apiKey;
}
