package ua.masterjoi.bsa_giphy.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class GiphyAppConfiguration {
    @Bean
    @Scope("singleton")
    public GiphyApp giphyApp(GiphyAppConfigurationProperties configurationProperties) {
        return new GiphyApp(configurationProperties.getUrl()
                , configurationProperties.getApiKey());
    }
}
