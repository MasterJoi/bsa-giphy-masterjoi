package ua.masterjoi.bsa_giphy.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ua.masterjoi.bsa_giphy.config.FilesStorage;
import ua.masterjoi.bsa_giphy.exception.SomethingNotFoundExeption;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class GifsRepository {

    @Autowired
    private FilesStorage filesStorage;


    public List<String> getAllGifs() {
        List<String> gifsList;
        try {
            gifsList = (Files.walk(Paths.get(filesStorage.getCacheDirectory()))
                    .filter(Files::isRegularFile)
                    .map(Path::normalize)
                    .map(Path::toString)
                    .collect(Collectors.toList()));
        } catch (IOException e) {
            throw new SomethingNotFoundExeption("Cache is missing!");
        }
        return gifsList;
    }
}
