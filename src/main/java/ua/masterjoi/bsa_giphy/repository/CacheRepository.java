package ua.masterjoi.bsa_giphy.repository;

import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import ua.masterjoi.bsa_giphy.config.FilesStorage;
import ua.masterjoi.bsa_giphy.exception.DeleteExeption;
import ua.masterjoi.bsa_giphy.exception.SomethingNotFoundExeption;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

@Repository
public class CacheRepository {

    @Autowired
    private FilesStorage filesStorage;

    public ResponseEntity<List<Map<String, Object>>> getCacheByQuery(String checkedQuery) {
        List<Map<String, Object>> list = new ArrayList<>();
        File rootFile = new File(filesStorage.getCacheDirectory() + "/" + checkedQuery);
        List<File> files;
        try {
            files = Arrays.asList((rootFile.listFiles()));
        } catch (NullPointerException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        if (!files.isEmpty()) {
            list.add(new HashMap<>(Map.of("query", checkedQuery)));
        } else {
            throw new SomethingNotFoundExeption("Can`t find cache file!");
        }

        try {
            list.get(0).put("gifs", Files
                    .walk(Paths.get(filesStorage.getCacheDirectory() + "/" + checkedQuery))
                    .filter(Files::isRegularFile)
                    .map(Path::normalize)
                    .map(Path::toString)
                    .collect(Collectors.toList()));
        } catch (IOException e) {
            throw new SomethingNotFoundExeption("Cache is missing!");
        }
        return ResponseEntity.status(HttpStatus.OK).body(list);
    }

    public ResponseEntity<List<Map<String, Object>>> getAllCache() {
        List<Map<String, Object>> list = new ArrayList<>();
        File rootFile = new File(filesStorage.getCacheDirectory());
        List<File> files = Arrays.asList(Objects.requireNonNull(rootFile.listFiles()));
        if (!files.isEmpty()) {
            for (File file : files) {
                list.add(new HashMap<>(Map.of("query", file.getName())));
            }
        } else {
            throw new SomethingNotFoundExeption("Can`t find cache file!");
        }

        for (Map<String, Object> map : list) {
            try {
                map.put("gifs", Files
                        .walk(Paths.get(filesStorage.getCacheDirectory() + "/" + map.get("query")))
                        .filter(Files::isRegularFile)
                        .map(Path::normalize)
                        .map(Path::toString)
                        .collect(Collectors.toList()));
            } catch (IOException e) {
                throw new SomethingNotFoundExeption("Cache is missing!");
            }
        }
        return ResponseEntity.status(HttpStatus.OK).body(list);
    }

    public Optional<String> getGifFromCacheByQuery(String checkedQuery) {
        File cacheQueryFolder = new File(filesStorage.getCacheDirectory()
                + "/" + checkedQuery);
        File[] gifs = cacheQueryFolder.listFiles();
        if (gifs != null) {
            int randomIndex = ThreadLocalRandom.current().nextInt(0, gifs.length);
            return Optional.of(gifs[randomIndex].getPath());
        }
        return Optional.empty();
    }

    public void deleteCache() {
        File cache = new File(filesStorage.getCacheDirectory());
        try {
            FileUtils.cleanDirectory(cache);
        } catch (Exception ex) {
            throw new DeleteExeption("Failed delete cache contents!");
        }
    }
}
