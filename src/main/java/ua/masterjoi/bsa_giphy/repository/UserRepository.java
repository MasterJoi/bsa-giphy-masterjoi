package ua.masterjoi.bsa_giphy.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import ua.masterjoi.bsa_giphy.config.CacheInMemory;
import ua.masterjoi.bsa_giphy.config.FilesStorage;
import ua.masterjoi.bsa_giphy.exception.DeleteExeption;
import ua.masterjoi.bsa_giphy.exception.SomethingNotFoundExeption;
import ua.masterjoi.bsa_giphy.service.GifsService;
import ua.masterjoi.bsa_giphy.service.UserFolderService;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

@Repository
public class UserRepository {

    @Autowired
    private FilesStorage filesStorage;

    @Autowired
    private UserFolderService userFolderService;

    @Autowired
    private CacheInMemory cache;

    @Autowired
    private GifsService gifsService;

    public ResponseEntity<List<Map<String, Object>>> getAllFilesFromUser(String user_id) {
        List<Map<String, Object>> list = new ArrayList<>();
        File rootFile = new File(filesStorage.getUsersDirectory() + "/" + user_id);
        List<File> files;
        try {
            files = Arrays.asList((rootFile.listFiles()));
        } catch (NullPointerException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        if (!files.isEmpty()) {
            for (File file : files) {
                list.add(new HashMap<>(Map.of("query", file.getName())));
            }
        } else {
            throw new SomethingNotFoundExeption("No files in user: " + user_id + " folder!");
        }

        for (Map<String, Object> map : list) {
            try {
                map.put("gifs", Files
                        .walk(Paths.get(filesStorage.getUsersDirectory() + "/" + user_id + "/" + map.get("query")))
                        .filter(Files::isRegularFile)
                        .map(Path::normalize)
                        .map(Path::toString)
                        .collect(Collectors.toList()));
            } catch (IOException e) {
                throw new SomethingNotFoundExeption("User " + user_id + " folder is missing!");
            }
        }
        return ResponseEntity.status(HttpStatus.OK).body(list);
    }

    public ResponseEntity<List<Map<String, String>>> getUserHistory(String user_id) {
        return ResponseEntity.status(HttpStatus.OK).body(userFolderService.getCsvData(user_id));
    }

    public void cleanUserHistory(String user_id) {
        try {
            Files.deleteIfExists(Paths.get(String.valueOf(userFolderService.getUserHistoryPath(user_id))));
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            Files.createFile(Paths.get(String.valueOf(userFolderService.getUserHistoryPath(user_id))));
        } catch (IOException e) {
            throw new DeleteExeption("Can`t clean the history!");
        }
    }

    public Optional<String> findUserGifFromCache(String user_id, String query) {
        return cache.getGifPath(user_id, query);
    }

    public Optional<String> findUserGifFromDirectory(String user_id, String query) {
        return userFolderService.getGifPath(user_id, query);
    }

    public Optional<String> getGifFromGiphy(String user_id, String query) {
        var gif = gifsService.findGif(user_id, query);
        return gifsService.downloadGifToCache(gif);
    }

    public void clearAllUserCache(String user_id) {
        cache.clearAllUserCache(user_id);
    }

    public void clearUserCacheByQuery(String user_id, String query) {
        cache.clearUserCacheByQuery(user_id, query);
    }
}
