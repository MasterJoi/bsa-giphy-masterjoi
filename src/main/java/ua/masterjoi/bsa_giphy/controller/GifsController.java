package ua.masterjoi.bsa_giphy.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ua.masterjoi.bsa_giphy.service.GifsService;

import java.util.List;

@RestController
@RequestMapping("/gifs")
public class GifsController {
    private static final Logger logger = LoggerFactory.getLogger(CacheController.class);

    @Autowired
    private GifsService gifsService;

    @GetMapping
    public List<String> getAllGifs() {
        logger.info("Request to get all gifs from disk");
        return gifsService.getAllGifs();
    }
}
