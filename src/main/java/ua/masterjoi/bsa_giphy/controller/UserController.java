package ua.masterjoi.bsa_giphy.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ua.masterjoi.bsa_giphy.exception.SomethingNotFoundExeption;
import ua.masterjoi.bsa_giphy.service.UserService;
import ua.masterjoi.bsa_giphy.util.UserIdValidator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/user")
public class UserController {
    private static final Logger logger = LoggerFactory.getLogger(CacheController.class);

    @Autowired
    private UserService userService;

    @Autowired
    private UserIdValidator userIdValidator;

    @GetMapping("/{id}/all")
    public ResponseEntity<List<Map<String, Object>>> getAllFilesFromUser(@Validated @PathVariable String id) {
        if(userIdValidator.isValidId(id)) {
            logger.info("Request to receive all files from user folder with id : " + id);
            return userService.getAllFilesFromUser(id);

        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/{id}/history")
    public ResponseEntity<List<Map<String, String>>> getUserHistory(@PathVariable String id) {
        if(userIdValidator.isValidId(id)) {
            logger.info("Request to receive history of user with id : " + id);
            return userService.getUserHistory(id);
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

    }

    @DeleteMapping("/{id}/history/clean")
    public void cleanUserHistory(@PathVariable String id) {
        if(userIdValidator.isValidId(id)) {
            logger.info("Request to clear history of user with id : " + id);
            userService.cleanUserHistory(id);
        }
    }

    @PostMapping("/{id}/generate")
    public String generateUserGif(@PathVariable String id, @RequestParam(required = false) Boolean force, @RequestParam String query) {
        if(userIdValidator.isValidId(id)) {
            logger.info("Request to generate gif for user with id : " + id);
            return userService.generateUserGif(force, query, id);

        } else {
            throw new SomethingNotFoundExeption("Invalid user id!");
        }

    }

    @GetMapping("/{id}/search")
    public ResponseEntity<String> findUserGif(@PathVariable String id, @RequestParam(required = false) Boolean force, @RequestParam String query) {
        if(userIdValidator.isValidId(id)) {
            logger.info("Request to find gif for user with id : " + id);
            return userService.findUserGif(force, query, id);

        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/{id}/clean")
    public void deleteAllUserData(@PathVariable String id) {
        if(userIdValidator.isValidId(id)) {
            logger.info("Request to delete user cache and folder with id : " + id);
            userService.deleteAllUserData(id);
        }

    }

    @DeleteMapping("/{id}/reset")

    public void resetUserCache(@PathVariable String id, @RequestParam(required = false) String query) {
        if(userIdValidator.isValidId(id)) {
            logger.info("Request to clear user cache with query : " + query);
            userService.clearUserCache(id, query);
        }
    }


}
