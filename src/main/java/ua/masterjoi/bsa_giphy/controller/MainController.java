package ua.masterjoi.bsa_giphy.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@Controller

public class MainController {
    @Value("${server.port}")
    int port;

    @ResponseBody
    @RequestMapping(path = "/")
    public String home(HttpServletRequest request) {

        String contextPath = request.getContextPath();
        String host = request.getServerName();

        String endpointBasePath = "/actuator";

        StringBuilder sb = new StringBuilder();

        sb.append("<h2>Sprig Boot Actuator</h2>");
        sb.append("<ul>");

        String url = "http://" + host + ":" + port + contextPath + endpointBasePath;

        sb.append("<li><a href='").append(url).append("'>").append(url).append("</a></li>");

        sb.append("</ul>");

        return sb.toString();
    }
}
