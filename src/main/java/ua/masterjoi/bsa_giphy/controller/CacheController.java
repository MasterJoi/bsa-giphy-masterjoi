package ua.masterjoi.bsa_giphy.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ua.masterjoi.bsa_giphy.service.CacheService;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/cache")
public class CacheController {
    private static final Logger logger = LoggerFactory.getLogger(CacheController.class);

    @Autowired
    private CacheService cacheService;

    @GetMapping
    public ResponseEntity<List<Map<String, Object>>> getCacheFromDisk(@RequestParam(required = false) String query) {
        String checkedQuery = Optional.ofNullable(query).orElse("");
        logger.info("Request to recieve all cache from disk with query: " + checkedQuery);

        return cacheService.getCacheFromDisk(checkedQuery);
    }

    @PostMapping("/generate")
    public ResponseEntity<List<Map<String, Object>>> downloadGif(@RequestParam String query) {
        logger.info("Request to download gif to cache with query: " + query);
        return cacheService.downloadGif(query);
    }

    @DeleteMapping
    public void deleteCache() {
        logger.info("Request to delete cache");
        cacheService.deleteCache();
    }


}
