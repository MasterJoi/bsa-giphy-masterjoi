package ua.masterjoi.bsa_giphy.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Gif {
    private String giphyUrl;
    private String id;
    private String query;
}
