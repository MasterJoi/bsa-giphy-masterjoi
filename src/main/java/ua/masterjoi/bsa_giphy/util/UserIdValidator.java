package ua.masterjoi.bsa_giphy.util;

import org.springframework.stereotype.Component;

import java.util.regex.Pattern;

@Component
public class UserIdValidator {
    public Boolean isValidId(String user_id) {
        Pattern pattern = Pattern.compile(".*[!@#$%^&:/()+.,~`|*?<>\"].*");
        return !pattern.matcher(user_id).find() && !user_id.isEmpty();
    }
}
