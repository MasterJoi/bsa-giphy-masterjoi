package ua.masterjoi.bsa_giphy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.context.annotation.PropertySource;
import ua.masterjoi.bsa_giphy.config.CacheInMemory;
import ua.masterjoi.bsa_giphy.config.FilesStorage;
import ua.masterjoi.bsa_giphy.config.GiphyApp;

@SpringBootApplication
@ConfigurationPropertiesScan
@PropertySource(value = "classpath:dev-application.properties")
public class BsaGiphyApplication {

    public static void main(String[] args) {
        var context = SpringApplication.run(BsaGiphyApplication.class, args);

        var filesStorage = context.getBean(FilesStorage.class);
        context.getBean(GiphyApp.class);
        context.getBean(CacheInMemory.class);
        filesStorage.createEmptyDirectories();
    }

}
