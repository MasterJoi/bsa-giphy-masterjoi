package ua.masterjoi.bsa_giphy.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@AllArgsConstructor
@Getter
@Setter
public class HistoryDTO {
    private LocalDate date;
    private String query;
    private String gif;
}
