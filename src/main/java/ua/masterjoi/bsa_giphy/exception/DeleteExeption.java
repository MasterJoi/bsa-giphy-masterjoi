package ua.masterjoi.bsa_giphy.exception;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DeleteExeption extends RuntimeException{
    public DeleteExeption(String message){
        super(message);
        log.error(message);
    }
}
