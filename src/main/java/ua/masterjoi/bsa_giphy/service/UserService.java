package ua.masterjoi.bsa_giphy.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import ua.masterjoi.bsa_giphy.repository.CacheRepository;
import ua.masterjoi.bsa_giphy.repository.UserRepository;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CacheRepository cacheRepository;

    @Autowired
    private UserFolderService userFolderService;

    public ResponseEntity<List<Map<String, Object>>> getAllFilesFromUser(String user_id) {
        return userRepository.getAllFilesFromUser(user_id);
    }

    public ResponseEntity<List<Map<String, String>>> getUserHistory(String user_id) {
        return userRepository.getUserHistory(user_id);
    }

    public void cleanUserHistory(String user_id) {
        userRepository.cleanUserHistory(user_id);
    }

    public ResponseEntity<String> findUserGif(Boolean force, String query, String user_id) {
        Optional<String> userGif;
        if (force != null) {
            userGif = userRepository.findUserGifFromDirectory(user_id, query);

        } else {
            userGif = userRepository.findUserGifFromCache(user_id, query);
            if (userGif.isEmpty()) {
                userGif = userRepository.findUserGifFromDirectory(user_id, query);
            }
        }
        return userGif.map(s -> ResponseEntity.status(HttpStatus.OK).
                body(s)).orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    public String generateUserGif(Boolean force, String query, String user_id) {
        Optional<String> gifCachePath;
        if (force != null) {
            gifCachePath = userRepository.getGifFromGiphy(user_id, query);
        } else {
            gifCachePath = cacheRepository.getGifFromCacheByQuery(query);

            if (gifCachePath.isEmpty()) {
                gifCachePath = userRepository.getGifFromGiphy(user_id, query);
            }
        }

        gifCachePath.ifPresent(gifPath -> userFolderService.addGifToUserFolder(user_id, query, gifPath));

        return gifCachePath.get();
    }

    public void clearUserCache(String user_id, String query) {
        if (query != null) {
            userRepository.clearUserCacheByQuery(user_id, query);
        } else {
            userRepository.clearAllUserCache(user_id);
        }
    }

    public void deleteAllUserData(String user_id) {
        userRepository.clearAllUserCache(user_id);
        userFolderService.deleteUserFolder(user_id);
    }
}
