package ua.masterjoi.bsa_giphy.service;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.masterjoi.bsa_giphy.config.FilesStorage;
import ua.masterjoi.bsa_giphy.config.GiphyApp;
import ua.masterjoi.bsa_giphy.entity.Gif;
import ua.masterjoi.bsa_giphy.exception.SomethingNotFoundExeption;
import ua.masterjoi.bsa_giphy.repository.GifsRepository;

import java.io.*;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;

@Service
public class GifsService {

    @Autowired
    private GiphyApp giphyApp;

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private FilesStorage filesStorage;

    @Autowired
    private GifsRepository gifsRepository;

    public Optional<Gif> findGif(String user_id, String query) {
        try {
            URL gifUrl = new URL(giphyApp.getUrl() + "?api_key=" + giphyApp.getApiKey() + "&tag=" + query + "&random_id=" + user_id);

            JsonNode root = mapper
                    .readTree(gifUrl)
                    .path("data");
            String source = root.get("images").get("downsized").get("url").asText();
            source = source.replace(source.substring(8, 14), "i");
            Gif gif = new Gif();
            gif.setId(root.get("id").asText());
            gif.setGiphyUrl(source);
            gif.setQuery(query);
            return Optional.of(gif);
        } catch (IOException e) {
            throw new SomethingNotFoundExeption("Gif source not found!");
        }
    }

    public Optional<String> downloadGifToCache(Optional<Gif> gif) {
        Optional<String> gifFilePath;
        if (gif.isPresent()) {
            Gif inputGif = gif.get();

            try (var inputStream = new BufferedInputStream(new URL(inputGif.getGiphyUrl()).openStream())) {
                File gifFolder = new File(filesStorage.getCacheDirectory() + "\\" + inputGif.getQuery());
                Files.createDirectories(Paths.get(gifFolder.getAbsolutePath()));

                File gifFile = new File(gifFolder, inputGif.getId() + ".gif");
                Files.copy(inputStream, gifFile.toPath());
                gifFilePath = Optional.of(gifFile.getPath());
                return gifFilePath;
            } catch (Exception ex) {
                throw new RuntimeException("Can`t download gif to cache!");
            }
        }
        return Optional.empty();
    }

    public List<String> getAllGifs() {
        return gifsRepository.getAllGifs();
    }
}
