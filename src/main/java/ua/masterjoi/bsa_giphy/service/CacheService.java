package ua.masterjoi.bsa_giphy.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import ua.masterjoi.bsa_giphy.repository.CacheRepository;

import java.util.List;
import java.util.Map;

@Service
public class CacheService {

    @Autowired
    private CacheRepository cacheRepository;

    @Autowired
    private GifsService gifsService;

    public ResponseEntity<List<Map<String, Object>>> getCacheFromDisk(String checkedQuery) {
        return !checkedQuery.isEmpty()? cacheRepository.getCacheByQuery(checkedQuery) : cacheRepository.getAllCache();
    }

    public ResponseEntity<List<Map<String, Object>>> downloadGif(String query) {

        var gif = gifsService.findGif(null, query);
        gifsService.downloadGifToCache(gif);
        return cacheRepository.getAllCache();
    }

    public void deleteCache() {
        cacheRepository.deleteCache();
    }
}
