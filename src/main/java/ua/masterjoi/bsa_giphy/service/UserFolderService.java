package ua.masterjoi.bsa_giphy.service;

import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.masterjoi.bsa_giphy.config.CacheInMemory;
import ua.masterjoi.bsa_giphy.config.FilesStorage;
import ua.masterjoi.bsa_giphy.dto.HistoryDTO;
import ua.masterjoi.bsa_giphy.exception.DeleteExeption;
import ua.masterjoi.bsa_giphy.exception.SomethingNotFoundExeption;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

@Service
public class UserFolderService {

    @Autowired
    private FilesStorage filesStorage;

    @Autowired
    private CacheInMemory cache;

    public File getUserHistoryPath(String user_id) {
        return new File(filesStorage.getUsersDirectory() + "\\" + user_id + "\\history.csv");
    }

    public List<Map<String, String>> getCsvData(String user_id) {
        try (BufferedReader csvReader = new BufferedReader(new FileReader(getUserHistoryPath(user_id)))) {
            String row;
            List<Map<String, String>> userHistory = new ArrayList<>();
            while ((row = csvReader.readLine()) != null) {
                String[] data = row.split(",");
                userHistory.add(Map.of("date", data[0], "query", data[1], "gif", data[2]));
            }
            return userHistory;
        } catch (IOException e) {
            throw new RuntimeException("Can`t get history data!");
        }
    }

    public Optional<String> getGifPath(String user_id, String query) {
        File userQueryFolder = new File(filesStorage.getUsersDirectory()
                + "/" + user_id + "/" + query);
        File[] gifs = userQueryFolder.listFiles();
        if (gifs != null) {
            int randomIndex = ThreadLocalRandom.current().nextInt(0, gifs.length);
            return Optional.of(userQueryFolder.getPath() + "\\" + gifs[randomIndex].getName());
        }
        return Optional.empty();
    }

    public void addGifToUserFolder(String user_id, String query, String gifPath) {
        File source = new File(gifPath);
        File target = new File(filesStorage.getUsersDirectory() + "/" + user_id + "/" + query);
        try {
            target.mkdirs();

            target = new File(target, source.getName());
            Files.copy(source.toPath(), target.toPath(), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            throw new RuntimeException("Can`t copy from cache to user folder!");
        }

        cache.addGifToCache(user_id, query, gifPath);
        updateHistoryFile(user_id, query, gifPath);
    }

    private void updateHistoryFile(String user_id, String query, String gifPath) {
        HistoryDTO historyDTO = new HistoryDTO(LocalDate.now(), query, gifPath);
        try (PrintWriter writer = new PrintWriter(new FileWriter(getUserHistoryPath(user_id), true))) {
            String sb = String.valueOf(historyDTO.getDate()) +
                    ',' +
                    historyDTO.getQuery() +
                    ',' +
                    historyDTO.getGif() +
                    '\n';
            writer.write(sb);
        } catch (IOException e) {
            throw new SomethingNotFoundExeption("History file doesn't exist!");
        }
    }

    public void deleteUserFolder(String user_id) {
        File userFolder = new File(filesStorage.getUsersDirectory() + "/" + user_id);
        try {
            FileUtils.cleanDirectory(userFolder);
        } catch (Exception ex) {
            throw new DeleteExeption("Failed delete folder contents!");
        }
    }
}
